#include "robotsystemstats.h"

#include <Core/Containers/skarraycast.h>
#include <Core/System/Info/skstatgrab.h>

ConstructorImpl(RobotSystemStats, SkFlowSat)
{
    procFs = nullptr;
    hostInfo = nullptr;
    cpuStat = nullptr;
    procStat = nullptr;
    memStat = nullptr;
    swapStat = nullptr;

    setObjectName("RobotSystemStats");

    procFs = new SkProcFs;
    procFs->setObjectName(this, "ProcFs");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotSystemStats::onSetup()
{
    return true;
}

void RobotSystemStats::onInit()
{
    AssertKiller(!procFs->grabHostInfo()
                 || !procFs->grabSystemStat()
                 || !procFs->grabProcessStat()
                 || !procFs->grabMemoryStat()
                 || !procFs->grabSwapStat());

    hostInfo = procFs->hostInfo();
    cpuStat = procFs->systemStat();
    procStat = procFs->processesStat();
    memStat = procFs->memoryStat();
    swapStat = procFs->swapStat();

    setVariable("osName", hostInfo->osName);
    setVariable("osRelease", hostInfo->osRelease);
    setVariable("osVersion", hostInfo->osVersion);
    setVariable("platform", hostInfo->platform);
    setVariable("hostName", hostInfo->hostName);
    setVariable("upTime", hostInfo->upTime);
    setVariable("processors", hostInfo->processors);
    setVariable("processorsOnLine", hostInfo->processorsOnLine);
    setVariable("bitsWidth", hostInfo->bitsWidth);

    procFs->grabNetworkStat();
    procFs->grabFsInfo();

    SkStringList interfaces;
    procFs->networkInterfacesNames(interfaces);
    setVariable("networks", interfaces);

    SkStringList fileSystems;
    procFs->fileSystemsNames(fileSystems);
    setVariable("storages", fileSystems);
}

void RobotSystemStats::onQuit()
{
    delete procFs;

    procFs = nullptr;
    hostInfo = nullptr;
    cpuStat = nullptr;
    procStat = nullptr;
    memStat = nullptr;
    swapStat = nullptr;
}

static float giga_divider = 1024*1024*1024;
static float mega_divider = 1024*1024;
//static float kilo_divider = 1024;

void RobotSystemStats::onOneSecTick()
{
    if (procFs->grabSystemStat())
    {
        setVariable("userTime", cpuStat->user.percent);
        setVariable("kernelTime", cpuStat->kernel.percent);
        setVariable("ioTime", cpuStat->iowait.percent);
        setVariable("swapTime", cpuStat->swap.percent);
        setVariable("niceTime", cpuStat->nice.percent);
        setVariable("idleTime", cpuStat->idle.percent);

        setVariable("load1", cpuStat->load_1);
        setVariable("load5", cpuStat->load_5);
        setVariable("load15", cpuStat->load_15);
    }

    if (procFs->grabProcessStat())
    {
        setVariable("processesTotal", procStat->total);
        setVariable("processesRunning", procStat->running);
        setVariable("processesSleeping", procStat->sleeping);
        setVariable("processesStopped", procStat->stopped);
        setVariable("processesZombie", procStat->zombie);
    }

    if (procFs->grabMemoryStat())
    {
        setVariable("ramTotal", memStat->total/giga_divider);
        setVariable("ramUsed", memStat->used/giga_divider);
        setVariable("ramUsedPercent", memStat->usedPercent);
        setVariable("ramCache", memStat->cache/giga_divider);
        setVariable("ramFree", memStat->free/giga_divider);

        setVariable("pagesIn", memStat->pagesIn);
        setVariable("pagesOut", memStat->pagesOut);
    }

    if (procFs->grabSwapStat())
    {
        setVariable("swapTotal", swapStat->total/giga_divider);
        setVariable("swapUsed", swapStat->used/giga_divider);
        setVariable("swapUsedPercent", swapStat->usedPercent);
        setVariable("swapFree", swapStat->free/giga_divider);
    }

    if (procFs->grabNetworkStat())
    {
        SkBinaryTreeVisit<SkString, SkNetworkInterfaceInfo *> *itr = procFs->networkInterfacesIterator();

        SkVariantList tx;
        SkVariantList rx;

        while(itr->next())
        {
            SkNetworkInterfaceInfo *interface = itr->item().value();

            tx << (interface->tx/mega_divider);
            rx << (interface->rx/mega_divider);
        }

        setVariable("networks_tx", tx);
        setVariable("networks_rx", rx);

        delete itr;
    }

    if (procFs->grabFsInfo())
    {
        SkBinaryTreeVisit<SkString, SkFsInfo *> *itr = procFs->fileSystemsIterator();

        SkVariantList tx;
        SkVariantList rx;

        while(itr->next())
        {
            SkFsInfo *fs = itr->item().value();

            tx << (fs->tx/mega_divider);
            rx << (fs->rx/mega_divider);

        }

        setVariable("storages_tx", tx);
        setVariable("storages_rx", rx);

        delete itr;
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#ifndef ROBOTSYSTEMSTATS_H
#define ROBOTSYSTEMSTATS_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Core/System/skcli.h>

struct SkProcFs;
struct SkHostInfo;
struct SkSysStat;
struct SkMemoryStat;
struct SkSwapStat;
struct SkProcessesStat;


class RobotSystemStats extends SkFlowSat
{
    SkProcFs *procFs;
    SkHostInfo *hostInfo;
    SkSysStat *cpuStat;
    SkProcessesStat *procStat;
    SkMemoryStat *memStat;
    SkSwapStat *swapStat;

    public:
        Constructor(RobotSystemStats, SkFlowSat);

    private:
        bool onSetup()              override;
        void onInit()               override;
        void onQuit()               override;

        void onOneSecTick()         override;
};

#endif // ROBOTSYSTEMSTATS_H
